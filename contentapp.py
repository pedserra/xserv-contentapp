#!/usr/bin/env python3

import webapp

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""

class ContentApp(webapp.webApp):
    dict_contents = {'/': "<p>pagina principal</p>",
                '/hola': "<p>Hola gente</p>",
                '/adios': "<p>adios gente!!</p>"}

    def parse (self, request):
        return request.split(' ',2)[1]

    def process (self, resource):
        if resource in self.dict_contents:
            content = self.dict_contents[resource]
            page = PAGE.format(content=content)
            code = "200 OK"
        else:
            page = PAGE_NOT_FOUND.format(resource=resource)
            code = "404 Resource Not Found"
        return (code, page)

if __name__ == "__main__":
    webApp = ContentApp ("localhost", 1234)